package main

import (
	"bufio"
	"flag"
	"fmt"
	"os"
	"os/exec"
	"strconv"
	"strings"
)

func initFlags(fileName *string) {
	flag.StringVar(fileName, "filename", "problems", "enter your csv file name like this => problems")
	flag.Parse()
}

func main() {
	var fileName string
	initFlags(&fileName)

	quizData, err := csvParser(fileName)
	if err != nil {
		fmt.Println(err.Error())
	} else {
		for try := true; try; {
			var correct int
			var incorrect int
			var totalQ int
			fmt.Println("Please answer the given questions !")
			for question, answer := range quizData {
				fmt.Printf("%s = ? \nYour answer : \n", question)
				ans := inputInt()
				if ans == answer {
					correct++
				} else {
					incorrect++
				}
				totalQ++
			}
			fmt.Printf("Total amount of questions : %d\n", totalQ)
			fmt.Printf("True answers : %d\n", correct)
			fmt.Printf("False answers : %d\n", incorrect)
			for true {
				fmt.Println("Do you want to try again ? (yes,no) : ")
				des := inpuStr()
				if des == "yes" {
					cleaner()
					break
				} else if des == "no" {
					cleaner()
					fmt.Println("Okay... See you later !")
					try = false
					break
				} else {
					fmt.Println("Wrong typing!!! I am asking you again .")
				}
			}
		}
	}

}

func csvParser(fileName string) (map[string]int, error) {
	file, err := os.Open(fileName + ".csv")
	if err != nil {
		fmt.Printf("There no such a csv file named %s .\n Please ensure your file exist in main directory !\n", fileName)
		return nil, err
	} else {
		repo := make(map[string]int)

		scanner := bufio.NewScanner(file)
		scanner.Split(bufio.ScanLines)
		for scanner.Scan() {
			bt := strings.Split(scanner.Text(), ",")
			intV, _ := strconv.ParseInt(bt[1], 10, 32)
			repo[bt[0]] = int(intV)
		}
		return repo, nil
	}

}

func inputInt() (input int) {
	var text string
	scanner := bufio.NewScanner(os.Stdin)
	for c := 0; c < 1 && scanner.Scan(); c++ {
		text = scanner.Text()
	}
	input64, _ := strconv.ParseInt(text, 10, 32)
	input = int(input64)
	return
}

func inpuStr() (input string) {
	scanner := bufio.NewScanner(os.Stdin)
	for i := 0; i < 1 && scanner.Scan(); i++ {
		input = scanner.Text()

	}
	return
}

func cleaner() {
	c := exec.Command("clear")
	c.Stdout = os.Stdout
	c.Run()
}
